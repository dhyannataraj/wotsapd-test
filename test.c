
#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include <sqlite3.h>

#define MY_INT int

#define KEY_ID_SIZE 17  /*16 for the key id, 1 for \0 */

#define ID_LAST -1


sqlite3 *db = NULL;
int key_count_var = 0;
char * key_ids_var = NULL;
char ** key_users_var = NULL;

MY_INT ** signed_var = NULL; /* Array [0..KEY_COUNT-1] of poiners to ID_LAST-terminated arrays */

MY_INT key_count_f();

#define KEY_COUNT (key_count_var ? key_count_var : key_count_f())


#define DIE(...) (fprintf(stderr, __VA_ARGS__), db && sqlite3_close(db),  exit(1)) /* print error message, close db if it is opened, exit */



MY_INT key_count_f()
{
  int rc;
  sqlite3_stmt *res;

  rc = sqlite3_prepare_v2(db, "select count(*) from keys", -1, &res, 0); 

  if (rc != SQLITE_OK)
  {
    fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(db));
    sqlite3_close(db);
    exit(1);
  }

  rc = sqlite3_step(res);
     // FIXME check rc

  key_count_var = sqlite3_column_int(res, 0);

  sqlite3_finalize(res);

  return key_count_var;
}


int load_keys()
{
  int rc;
  sqlite3_stmt *stmt;
  int i;
  char * p;

  // FIXME die if already loaded

  key_ids_var = malloc(KEY_COUNT * KEY_ID_SIZE);
  key_users_var = malloc(KEY_COUNT * sizeof(char *));


  rc = sqlite3_prepare_v2(db, "select id, kid, uid from keys ORDER BY id", -1, &stmt, 0);

  if (rc != SQLITE_OK)
  {
    fprintf(stderr, "Failed to fetch data: %s\n", sqlite3_errmsg(db));
    sqlite3_close(db);
    exit(1);
  }
  p = key_ids_var;
  for (i = 0; i< KEY_COUNT; i++)
  {
     rc = sqlite3_step(stmt);
     // FIXME check rc
     if (i != sqlite3_column_int(stmt, 0))
       DIE("Wot data seems to be corrupted: ids should be 0,1,2...N without gaps");
    // FIXME check that key_id are ordered here 

     strcpy(p,(char *) sqlite3_column_text(stmt, 1));
     key_users_var[i] = strdup (sqlite3_column_text(stmt, 2));  // FIXME do not forget to free it at the end
//     printf("%i %i %s \t %s\n",i, sqlite3_column_int(stmt, 0) , p, key_users_var[i]);
     p = p + KEY_ID_SIZE;
  }
  sqlite3_finalize(stmt);
  return 1;
}

int load_sigs()
{
  int rc;
  sqlite3_stmt *stmt;
  int i;
  char * p;

  MY_INT * buf = NULL;
  int buf_size= -1;
  int buf_step = 256;
  int buf_count;

  int last_id;

  signed_var = malloc(sizeof(MY_INT) * KEY_COUNT);

  buf = malloc(sizeof(MY_INT) * buf_step);
  buf_size = buf_step;

  rc = sqlite3_prepare_v2(db, "SELECT src, dst FROM sigs ORDER BY src", -1, &stmt, 0);

  if (rc != SQLITE_OK)
    DIE("Failed to fetch data: %s\n", sqlite3_errmsg(db));

  rc = sqlite3_step(stmt);
   // FIXME check rc

  last_id = -1;
  for(i=0; i<KEY_COUNT; i++)
  {
    if (sqlite3_column_int(stmt, 0)>i)
    {
      signed_var[i] = NULL;
//printf("%i --> NONE\n",i);
      continue;
    }
    buf_count = 0;
//printf("%i(%i) --> ",i,buf_size);
    while(sqlite3_column_int(stmt, 0) == i)
    {
      if (buf_count >= buf_size)
      {
        buf_size += buf_step;
        buf = realloc(buf, buf_size * sizeof(MY_INT));
      }
      buf[buf_count] = sqlite3_column_int(stmt, 0);
//printf("%i, ",sqlite3_column_int(stmt, 1));
      buf_count++;
      rc = sqlite3_step(stmt);
      // FIXME check rc
    }
    signed_var[i] = malloc(sizeof(MY_INT)*(buf_count + 1));
    memcpy(signed_var[i], buf, sizeof(MY_INT)*buf_count);
    signed_var[i][buf_count] = ID_LAST;

//printf("\n");
  }
}


char * key_ids_f()
{
  if (key_ids_var) return key_ids_var;
  load_keys();
  return key_ids_var;
}





int main(void)
{
  printf("hw %i\n", sizeof(int));
   char *zErrMsg = 0;
   int rc;

   rc = sqlite3_open("wrk/wotsap.lite", &db);

   if( rc ) {
      fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
      return(0);
   } else {
      fprintf(stderr, "Opened database successfully\n");
   }


  key_count_f();
  key_ids_f();
  load_sigs();

  sqlite3_close(db);
}

